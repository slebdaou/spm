/**
 *
 * @file z_spm_2dense.c
 *
 * SParse Matrix package conversion to dense routine.
 *
 * @copyright 2016-2017 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Theophile Terraz
 * @author Alban Bellot
 * @date 2015-01-01
 *
 * @precisions normal z -> c d s
 *
 **/
#include "common.h"
#include "z_spm.h"

/**
 * @brief Convert to dense a diagonal element within a symmetric/hermitian
 * matrix with column/row major storage
 *
 * Note that column major is using the low triangular part only of the diagonal
 * element matrices, and row major, by symmetry, is using only the upper
 * triangular part.
 *
 * The comments in the code are made for column major storage.
 */
static inline void
z_spm_2dense_elt_sym_diag( const spm_int_t        row,
                           const spm_int_t        dofi,
                           const spm_zconj_fct_t  conjfct,
                           const spm_complex64_t *valptr,
                           spm_complex64_t       *A,
                           const spm_int_t        lda )
{
    spm_int_t ii, jj;

    for(jj=0; jj<dofi; jj++)
    {
        /* Skip unused upper triangular part */
        for(ii=0; ii<jj; ii++) {
            valptr++;
        }

        /* Diagonal element */
        A[ lda * (row + jj) + (row + ii) ] = *valptr;
        valptr++;

        for(ii=jj+1; ii<dofi; ii++, valptr++)
        {
            /* Lower part */
            A[ lda * (row + jj) + (row + ii) ] = *valptr;
            /* Upper part */
            A[ lda * (row + ii) + (row + jj) ] = conjfct(*valptr);
        }
    }
    (void)conjfct;
}

/**
 * @brief Convert to dense a general element matrix with column major storage
 */
static inline void
z_spm_2dense_elt_gen_col( const spm_int_t        row,
                          const spm_int_t        dofi,
                          const spm_int_t        col,
                          const spm_int_t        dofj,
                          const spm_zconj_fct_t  conjfct,
                          const spm_complex64_t *valptr,
                          spm_complex64_t       *A,
                          const spm_int_t        lda )
{
    spm_int_t ii, jj;

    for(jj=0; jj<dofj; jj++)
    {
        for(ii=0; ii<dofi; ii++, valptr++)
        {
            A[ lda * (col + jj) + (row + ii) ] = conjfct(*valptr);
        }
    }
}

/**
 * @brief Convert to dense a general element matrix with row major storage
 */
static inline void
z_spm_2dense_elt_gen_row( const spm_int_t        row,
                          const spm_int_t        dofi,
                          const spm_int_t        col,
                          const spm_int_t        dofj,
                          const spm_zconj_fct_t  conjfct,
                          const spm_complex64_t *valptr,
                          spm_complex64_t       *A,
                          const spm_int_t        lda )
{
    spm_int_t ii, jj;

    for(ii=0; ii<dofi; ii++)
    {
        for(jj=0; jj<dofj; jj++, valptr++)
        {
            A[ lda * (col + jj) + (row + ii) ] = conjfct(*valptr);
        }
    }
}

/**
 * @brief Convert to dense a general element matrix
 */
static inline void
z_spm_2dense_elt_gen( const spm_layout_t     layout,
                      const spm_int_t        row,
                      const spm_int_t        dofi,
                      const spm_int_t        col,
                      const spm_int_t        dofj,
                      const spm_zconj_fct_t  conjfct,
                      const spm_complex64_t *valptr,
                      spm_complex64_t       *A,
                      const spm_int_t        lda )
{
    if ( layout == SpmColMajor ) {
        z_spm_2dense_elt_gen_col( row, dofi, col, dofj, conjfct, valptr, A, lda );
    }
    else {
        z_spm_2dense_elt_gen_row( row, dofi, col, dofj, conjfct, valptr, A, lda );
    }
}

/**
 * @brief Convert to dense an off-diagonal element matrix in the
 * symmetric/hermitian case
 */
static inline void
z_spm_2dense_elt_sym_offd( const spm_layout_t     layout,
                           const spm_int_t        row,
                           const spm_int_t        dofi,
                           const spm_int_t        col,
                           const spm_int_t        dofj,
                           const spm_zconj_fct_t  conjfct,
                           const spm_complex64_t *valptr,
                           spm_complex64_t       *A,
                           const spm_int_t        lda )
{
    if ( layout == SpmColMajor ) {
        /* A[ row, col ] */
        z_spm_2dense_elt_gen_col( row, dofi, col, dofj, __spm_zid, valptr, A, lda );
        /*
         * A[ col, row ] = conj( A[ row, col ]^t )
         * => Let's exploit the row major kernel to make it transpose
         */
        z_spm_2dense_elt_gen_row( col, dofj, row, dofi, conjfct,   valptr, A, lda );
    }
    else {
        z_spm_2dense_elt_gen_row( row, dofi, col, dofj, __spm_zid, valptr, A, lda );
        z_spm_2dense_elt_gen_col( col, dofj, row, dofi, conjfct,   valptr, A, lda );
    }
}

/**
 * @brief Convert to dense an element matrix
 */
static inline void
z_spm_2dense_elt( const spm_mtxtype_t    mtxtype,
                  const spm_layout_t     layout,
                  const spm_int_t        row,
                  const spm_int_t        dofi,
                  const spm_int_t        col,
                  const spm_int_t        dofj,
                  const spm_complex64_t *valptr,
                  spm_complex64_t       *A,
                  const spm_int_t        lda )
{
    if ( mtxtype == SpmGeneral ) {
        z_spm_2dense_elt_gen( layout, row, dofi, col, dofj, __spm_zid, valptr, A, lda );
    }
    else {
        spm_zconj_fct_t conjfct;

#if defined(PRECISION_c) || defined(PRECISION_z)
        if ( mtxtype == SpmHermitian ) {
            conjfct = __spm_zconj;
        }
        else
#endif
        {
            conjfct = __spm_zid;
        }

        if ( row == col ) {
            assert( dofi == dofj );
            z_spm_2dense_elt_sym_diag( row, dofi, conjfct, valptr, A, lda );
        }
        else {
            z_spm_2dense_elt_sym_offd( layout, row, dofi, col, dofj, conjfct, valptr, A, lda );
        }
    }
}

/**
 *******************************************************************************
 *
 * @ingroup spm_dev_convert
 *
 * @brief Convert a CSC matrix into a dense matrix.
 *
 * The denses matrix is initialized with zeroes and filled with the spm matrix
 * values. When the matrix is hermitian or symmetric, both sides (upper and
 * lower) of the dense matrix are initialized.
 *
 *******************************************************************************
 *
 * @param[in] spm
 *          The sparse matrix in the CSC format.
 *
 *******************************************************************************
 *
 * @return A dense matrix in Lapack layout format
 *
 *******************************************************************************/
static inline spm_complex64_t *
z_spmCSC2dense( const spmatrix_t *spm )
{
    spm_int_t              j, k, lda, baseval;
    spm_int_t              ig, dofi, row;
    spm_int_t              jg, dofj, col;
    const spm_int_t       *colptr;
    const spm_int_t       *rowptr;
    const spm_int_t       *dofs;
    const spm_int_t       *loc2glob;
    const spm_complex64_t *valptr;
    spm_complex64_t       *A;

    assert( spm->fmttype == SpmCSC );
    assert( spm->flttype == SpmComplex64 );

    lda = spm->gNexp;
    A = (spm_complex64_t*)malloc(lda * lda * sizeof(spm_complex64_t));
    memset( A, 0, lda * lda * sizeof(spm_complex64_t));

    baseval = spmFindBase( spm );

    colptr   = spm->colptr;
    rowptr   = spm->rowptr;
    valptr   = (spm_complex64_t*)(spm->values);
    dofs     = spm->dofs;
    loc2glob = spm->loc2glob;

    for(j=0; j<spm->n; j++, colptr++, loc2glob++)
    {
        jg = (spm->loc2glob == NULL) ? j : (*loc2glob) - baseval;
        if ( spm->dof > 0 ) {
            dofj = spm->dof;
            col  = spm->dof * jg;
        }
        else {
            dofj = dofs[jg+1] - dofs[jg];
            col  = dofs[jg] - baseval;
        }

        for(k=colptr[0]; k<colptr[1]; k++, rowptr++)
        {
            ig = (*rowptr - baseval);
            if ( spm->dof > 0 ) {
                dofi = spm->dof;
                row  = spm->dof * ig;
            }
            else {
                dofi = dofs[ig+1] - dofs[ig];
                row  = dofs[ig] - baseval;
            }

            z_spm_2dense_elt( spm->mtxtype, spm->layout,
                              row, dofi, col, dofj, valptr,
                              A, lda );
            valptr += dofi * dofj;
        }
    }

    return A;
}

/**
 *******************************************************************************
 *
 * @ingroup spm_dev_convert
 *
 * @brief Convert a CSR matrix into a dense matrix.
 *
 * The denses matrix is initialized with zeroes and filled with the spm matrix
 * values. When the matrix is hermitian or symmetric, both sides (upper and
 * lower) of the dense matrix are initialized.
 *
 *******************************************************************************
 *
 * @param[in] spm
 *          The sparse matrix in the CSR format.
 *
 *******************************************************************************
 *
 * @return A dense matrix in Lapack layout format
 *
 *******************************************************************************/
static inline spm_complex64_t *
z_spmCSR2dense( const spmatrix_t *spm )
{
    spm_int_t              i, k, lda, baseval;
    spm_int_t              ig, dofi, row;
    spm_int_t              jg, dofj, col;
    const spm_int_t       *colptr;
    const spm_int_t       *rowptr;
    const spm_int_t       *dofs;
    const spm_int_t       *loc2glob;
    const spm_complex64_t *valptr;
    spm_complex64_t       *A;

    assert( spm->fmttype == SpmCSR );
    assert( spm->flttype == SpmComplex64 );

    lda = spm->gNexp;
    A = (spm_complex64_t*)malloc(lda * lda * sizeof(spm_complex64_t));
    memset( A, 0, lda * lda * sizeof(spm_complex64_t));

    baseval = spmFindBase( spm );

    colptr   = spm->colptr;
    rowptr   = spm->rowptr;
    valptr   = (spm_complex64_t*)(spm->values);
    dofs     = spm->dofs;
    loc2glob = spm->loc2glob;

    for(i=0; i<spm->n; i++, rowptr++, loc2glob++)
    {
        ig = (spm->loc2glob == NULL) ? i : (*loc2glob) - baseval;
        if ( spm->dof > 0 ) {
            dofi = spm->dof;
            row  = spm->dof * ig;
        }
        else {
            dofi = dofs[ig+1] - dofs[ig];
            row  = dofs[ig] - baseval;
        }

        for(k=rowptr[0]; k<rowptr[1]; k++, colptr++)
        {
            jg = (*colptr - baseval);
            if ( spm->dof > 0 ) {
                dofj = spm->dof;
                col  = spm->dof * jg;
            }
            else {
                dofj = dofs[jg+1] - dofs[jg];
                col  = dofs[jg] - baseval;
            }

            z_spm_2dense_elt( spm->mtxtype, spm->layout,
                              row, dofi, col, dofj, valptr,
                              A, lda );
            valptr += dofi * dofj;
        }
    }

    return A;
}

/**
 *******************************************************************************
 *
 * @ingroup spm_dev_convert
 *
 * @brief Convert a IJV matrix into a dense matrix.
 *
 * The denses matrix is initialized with zeroes and filled with the spm matrix
 * values. When the matrix is hermitian or symmetric, both sides (upper and
 * lower) of the dense matrix are initialized.
 *
 *******************************************************************************
 *
 * @param[in] spm
 *          The sparse matrix in the IJV format.
 *
 *******************************************************************************
 *
 * @return A dense matrix in Lapack layout format
 *
 *******************************************************************************/
static inline spm_complex64_t *
z_spmIJV2dense( const spmatrix_t *spm )
{
    spm_int_t              k, lda, baseval;
    spm_int_t              i, dofi, row;
    spm_int_t              j, dofj, col;
    const spm_int_t       *colptr;
    const spm_int_t       *rowptr;
    const spm_int_t       *dofs;
    const spm_complex64_t *valptr;
    spm_complex64_t       *A;

    assert( spm->fmttype == SpmIJV );
    assert( spm->flttype == SpmComplex64 );

    lda = spm->gNexp;
    A = (spm_complex64_t*)malloc(lda * lda * sizeof(spm_complex64_t));
    memset( A, 0, lda * lda * sizeof(spm_complex64_t));

    baseval = spmFindBase( spm );

    colptr = spm->colptr;
    rowptr = spm->rowptr;
    valptr = (spm_complex64_t*)(spm->values);
    dofs   = spm->dofs;

    for(k=0; k<spm->nnz; k++, rowptr++, colptr++)
    {
        i = *rowptr - baseval;
        j = *colptr - baseval;

        if ( spm->dof > 0 ) {
            dofi = spm->dof;
            row  = spm->dof * i;
            dofj = spm->dof;
            col  = spm->dof * j;
        }
        else {
            dofi = dofs[i+1] - dofs[i];
            row  = dofs[i] - baseval;
            dofj = dofs[j+1] - dofs[j];
            col  = dofs[j] - baseval;
        }

        z_spm_2dense_elt( spm->mtxtype, spm->layout,
                          row, dofi, col, dofj, valptr,
                          A, lda );
        valptr += dofi * dofj;
    }

    return A;
}

/**
 *******************************************************************************
 *
 * @ingroup spm_dev_convert
 *
 * @brief Convert a sparse matrix into a dense matrix.
 *
 * The denses matrix is initialized with zeroes and filled with the spm matrix
 * values. When the matrix is hermitian or symmetric, both sides (upper and
 * lower) of the dense matrix are initialized.
 *
 *******************************************************************************
 *
 * @param[in] spm
 *          The sparse matrix to convert in any format.
 *
 *******************************************************************************
 *
 * @return A dense matrix in Lapack layout format
 *
 *******************************************************************************/
spm_complex64_t *
z_spm2dense( const spmatrix_t *spm )
{
    spm_complex64_t *A;

    if ( spm->loc2glob != NULL ) {
        fprintf( stderr, "spm2dense: Conversion to dense matrix with distributed spm is not available\n");
        return NULL;
    }

    switch (spm->fmttype) {
    case SpmCSC:
        A = z_spmCSC2dense( spm );
        break;
    case SpmCSR:
        A = z_spmCSR2dense( spm );
        break;
    case SpmIJV:
        A = z_spmIJV2dense( spm );
        break;
    }

    return A;
}

/**
 *******************************************************************************
 *
 * @ingroup spm_dev_convert
 *
 * @brief Print a dense matrix to the given file
 *
 *******************************************************************************
 *
 * @param[in] f
 *          Open file descriptor on which to write the matrix.
 *
 * @param[in] m
 *          Number of rows of the matrix A.
 *
 * @param[in] n
 *          Number of columns of the matrix A.
 *
 *
 * @param[in] A
 *          The matrix to print of size lda -by- n
 *
 * @param[in] lda
 *          the leading dimension of the matrix A. lda >= m
 *
 *******************************************************************************/
void
z_spmDensePrint( FILE *f, spm_int_t m, spm_int_t n, const spm_complex64_t *A, spm_int_t lda )
{
    spm_int_t i, j;

    for(j=0; j<n; j++)
    {
        for(i=0; i<m; i++)
        {
            if ( cabs( A[ j * lda + i ] ) != 0. ) {
                z_spmPrintElt( f, i, j, A[lda * j + i] );
            }
        }
    }
    return;
}
